package com.bowling.kata.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Frame {

    private static final int TEN = 10;

    private int firstPins;
    private int secondPins;

    /**
     * check if we have a strike
     *
     * @return boolean
     */
    public boolean isStrike() {
        return firstPins == TEN;
    }

    /**
     * check if we have a spare
     *
     * @return boolean
     */
    public boolean isSpare() {
        return firstPins + secondPins == TEN;
    }

    /**
     * sum pins
     *
     * @return int
     */
    public int sumPins() {
        return firstPins + secondPins;
    }

}
