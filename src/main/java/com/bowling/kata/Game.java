package com.bowling.kata;

import com.bowling.kata.utils.Frame;
import com.bowling.kata.utils.Line;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Game {

    private static final int MAX_POINTS = 300;

    private int currentRoll;
    private Line line;

    /**
     * frame roll
     *
     * @param firstPins  - pins knocked down for the first roll
     * @param secondPins - pins knocked down for the second roll
     */
    public void frameRoll(int firstPins, int secondPins) {
        isValidGame();
        initializeFrame(firstPins, secondPins);
        currentRoll++;
    }

    /**
     * calculate total score of the game's line
     *
     * @return int
     */
    public int calculateScore() {
        int score = 0;

        for (int i = 0; i < line.getFrames().size() - 1; i++) {
            if (score < MAX_POINTS) {
                Frame frame = line.getFrames().get(i);

                if (frame.isStrike()) {
                    score += calculateStrikeScore(frame, i);
                }
                else if (frame.isSpare()) {
                    score += calculateSpareScore(frame, i);
                } else {
                    score += frame.sumPins();
                }
            }
        }

        return score;
    }

    /**
     * calculate strike score
     *
     * @param frame - frame
     * @param roll - roll
     * @return int
     */
    private int calculateStrikeScore(Frame frame, int roll) {
        Frame nextFrame = line.getFrames().get(roll + 1);
        int score = frame.sumPins() + nextFrame.sumPins();

        if (nextFrame.isStrike()) {
            nextFrame = line.getFrames().get(roll + 2);

            score += nextFrame.getFirstPins();
        }

        return score;
    }

    /**
     * calculate spare score
     *
     * @param frame - frame
     * @param roll - roll
     * @return int
     */
    private int calculateSpareScore(Frame frame, int roll) {
        Frame nextFrame = line.getFrames().get(roll + 1);

        return frame.sumPins() + nextFrame.getFirstPins();
    }

    /**
     * check if the game has valid params
     */
    private void isValidGame() {
        if (line == null) {
            throw new IllegalArgumentException("invalid bowling line.");
        }
    }

    /**
     * initialize frame
     *
     * @param firstPins  - pins knocked down for the first roll
     * @param secondPins - pins knocked down for the second roll
     */
    private void initializeFrame(int firstPins, int secondPins) {
        line.getFrames().add(currentRoll, new Frame(firstPins, secondPins));
    }

}
