package com.bowling.kata;

import static org.assertj.core.api.Assertions.*;

import com.bowling.kata.utils.Frame;
import com.bowling.kata.utils.Line;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class GameTest {

    private static final int FRAMES_SIZE = 12;

    private Game game;

    @Before
    public void init() {
        game = new Game();
    }

    @Test
    public void shouldThrowExceptionWhenLineIsNull() {
        // When
        // Then
        assertThatThrownBy(() -> game.frameRoll(0, 0))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("invalid bowling line.");
    }

    @Test
    public void shouldReturn0WhenNoPinsKnockedDown() {
        // Given
        initGameLine();

        // When
        initFrames(0, 0);

        // Then
        assertThat(game.calculateScore())
            .isZero();
    }

    @Test
    public void shouldReturn7WhenFirstPinsIs5AndSecondPinsIs2() {
        // Given
        initGameLine();

        // When
        game.frameRoll(5, 2);
        initFrames(1, 0);

        // Then
        assertThat(game.calculateScore())
            .isEqualTo(7);
    }

    @Test
    public void shouldReturn22WhenFirstPinsIs1AndSecondPinsIs1ForAllFrames() {
        // Given
        initGameLine();

        // When
        initFrames(0, 1);

        // Then
        assertThat(game.calculateScore())
            .isEqualTo(22);
    }

    @Test
    public void shouldReturn35WhenSpareFirstPinsIs5AndSecondPinsIs2() {
        // Given
        initGameLine();

        // When
        game.frameRoll(1, 4);
        game.frameRoll(4, 5);
        game.frameRoll(6, 4);
        game.frameRoll(5, 1);
        initFrames(4, 0);

        // Then
        assertThat(game.calculateScore())
            .isEqualTo(35);
    }

    @Test
    public void shouldReturn29WhenStrikeFirstPinsIs5AndSecondPinsIs2() {
        // Given
        initGameLine();

        // When
        game.frameRoll(4, 3);
        game.frameRoll(10, 0);
        game.frameRoll(5, 1);
        initFrames(0, 0);

        // Then
        assertThat(game.calculateScore())
            .isEqualTo(29);
    }

    /**
     * initialise game's line with the frame list
     */
    private void initGameLine() {
        game.setLine(new Line(new ArrayList<>(FRAMES_SIZE)));
    }

    /**
     * initialise game's frames
     *
     * @param index - index from where we start
     * @param value - pins value
     */
    private void initFrames(int index, int value) {
        for (int i = index; i < FRAMES_SIZE; i++) {
            game.getLine().getFrames().add(new Frame(value, value));
        }
    }

}
