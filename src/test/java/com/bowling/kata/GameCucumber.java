package com.bowling.kata;

import static org.assertj.core.api.Assertions.*;

import com.bowling.kata.utils.Frame;
import com.bowling.kata.utils.Line;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

@RunWith(Cucumber.class)
public class GameCucumber {

    private static final String HYPHEN_SEPARATOR = "-";
    private static final String COMMA_SEPARATOR = ",";
    private static final int FRAMES_SIZE = 12;

    private Game game;
    private int score;

    @Given("^initialise game line$")
    public void init_game() {
        game = new Game();
        initGameLine();
    }

    @When("call calculate score using {string}")
    public void calculate_score_for_sequences(String sequence) {
        initFrames(sequence);
        score = game.calculateScore();
    }

    @Then("should return {int}")
    public void shouldReturnScores(Integer expectedScore) {
        assertThat(score).isEqualTo(expectedScore);
    }

    /**
     * initialise game's line with the frame list
     */
    private void initGameLine() {
        game.setLine(new Line(new ArrayList<>(FRAMES_SIZE)));
    }

    /**
     * initialise frames from sequence
     *
     * @param sequence - sequence
     */
    private void initFrames(String sequence) {
        String[] framesStr = sequence.split(COMMA_SEPARATOR);

        Arrays.stream(framesStr)
            .filter(Objects::nonNull)
            .map(String::trim)
            .forEach(frame -> {
                String[] frameValues = frame.split(HYPHEN_SEPARATOR);

                game.getLine().getFrames()
                    .add(
                        new Frame(
                            Integer.parseInt(frameValues[0]),
                            Integer.parseInt(frameValues[1])
                        )
                    );
            });
    }

}
