Feature: test bowling-kata with many frames sequences
  Scenario Outline: calling calculateScore
    Given initialise game line
    When call calculate score using <sequence>
    Then should return <score>

    Examples:
      | sequence                                                                 | score  |
      | "1-4,  4-5,  6-4,  5-5,  10-0, 0-1,  7-3,  6-4,  10-0, 2-8,  6-0"        |  133   |
      | "5-5,  5-5,  5-5,  5-5,  5-5,  5-5,  5-5,  5-5,  5-5,  5-5,  5-0"        |  150   |
      | "9-0,  9-0,  9-0,  9-0,  9-0,  9-0,  9-0,  9-0,  9-0,  9-0,  0-0"        |  90    |
      | "10-0, 9-1,  5-5,  7-2,  10-0, 10-0, 10-0, 9-0,  8-2,  9-1,  10-0"       |  187   |
      | "8-2,  5-4,  9-0,  10-0, 10-0, 5-5,  5-3,  6-3,  9-1,  9-1,  10-0"       |  149   |
      | "10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0, 10-0" |  300   |
